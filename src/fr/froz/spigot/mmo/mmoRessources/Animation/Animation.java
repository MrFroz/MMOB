package fr.froz.spigot.mmo.mmoRessources.Animation;



import net.minecraft.server.v1_12_R1.EnumParticle;
import net.minecraft.server.v1_12_R1.PacketPlayOutWorldParticles;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;



public class Animation {
    public static void spawnParticle(Player p, EnumParticle particleType,Location l,float offSet,double speed,int amount){
        PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(particleType, true, (float) l.getX(),(float) l.getY(),(float) l.getZ(), offSet, offSet, offSet,(float) speed, amount);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
    }
}
