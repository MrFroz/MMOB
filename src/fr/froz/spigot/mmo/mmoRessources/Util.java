package fr.froz.spigot.mmo.mmoRessources;

import fr.froz.spigot.mmo.mmoRessources.customMob.Hitbox;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;

import java.util.ArrayList;


public class Util {

    /**
     * @param player entity
     * @return the location front of the player
     */
    public static Location getFrontLocation(Entity player) {
       Location location = player.getLocation();
        Vector vec = player.getLocation().getDirection();
        vec.setY(0);
        location.add(vec);
        return location;
    }

    /**
     * @param player Living entity
     * @param hitbox The hitbox
     * @return entity in sight
     */
    public static Entity getNearestEntityInSight(LivingEntity player, Hitbox hitbox) {
        ArrayList<Entity> entities = (ArrayList<Entity>) player.getNearbyEntities(hitbox.getHitDistance(), hitbox.getHitDistance(), hitbox.getHitDistance());
        ArrayList<Block> sightBlock = (ArrayList<Block>) player.getLineOfSight(null, (int) hitbox.getHitDistance());
        ArrayList<Location> sight = new ArrayList<>();
        for (Block aSightBlock : sightBlock) sight.add(aSightBlock.getLocation());
        for (Location aSight : sight) {
            for (Entity entity : entities) {
                if (Math.abs(entity.getLocation().getX() - aSight.getX()) < hitbox.getX()) {
                    if (Math.abs(entity.getLocation().getY()+hitbox.getCenterY() - aSight.getY()) < hitbox.getY()) {
                        if (Math.abs(entity.getLocation().getZ() - aSight.getZ()) < hitbox.getZ()) {
                            return entity;
                        }
                    }
                }
            }
        }
        return null;
    }


}
